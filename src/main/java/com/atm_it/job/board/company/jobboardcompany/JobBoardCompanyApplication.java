package com.atm_it.job.board.company.jobboardcompany;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobBoardCompanyApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobBoardCompanyApplication.class, args);
    }

}
